
### 功能设计

1. 渲染引擎

   创建上下文（宽，高）
   把上下文元素 传入到几何图形， 变换 里面
   返回一个渲染引擎

3. 数据映射
    几何图形 通道
    
    ​	 创建元素、设置属性和挂载元素。 
    
3. 比例尺

4. 坐标刻度

5. 辅助工具



### 渲染引擎

- 
- 

### 创建上下文



### 比例尺

- 恒等比例尺

```javascript
export function createIdentity() {
    return (x) => x;
}
```



- 线性比例尺

  从 *[0.1, 1.7, 3.4, 5, 6.6, 8.3];* 映射到 *[0,2,4,6,8]* 

  解决三个问题：均匀的步长，刻度可读性，最大最小值

  
  1. 先处理映射关系
  
     传入两个范围后，返回的函数可以处理值
  
  ```javascript
  const scale = createLinear({
      domain: [0, 1], // 输入的范围是 [0, 1]
      range: [0, 10], // 输出的范围是 [0, 10]
  })
  scale(0.2); // 2
  scale(0.5); // 5
  
  ```
  
  ```javascript
  export function createLinear({
      domain: [d0, d1],
      range: [r0, r1],
      interpolate = interpolateNumber
  }) {
      return (x) => {
          const t = normalize(x, d0, d1) // 得到输入的值到两端的比例 归一化
          return interpolate(t, r0, r1) // 返回在映射在range值域内地值
      }
  }
  
  export function interpolateNumber(t, start, stop) {
      return start * (1 - t) + stop * t
  }
  
  // 顺便处理一下颜色，用作渐变
  export function interpolateColor(t, start, stop) {
      const r = interpolateNumber(t,start[0],stop[1])
      const g = interpolateNumber(t,start[0],stop[1])
      const b = interpolateNumber(t,start[0],stop[1])
      return `rgb(${r},${g},${b})`
  }
  ```
  
  
  
  2. 生成坐标刻度
  
     获得刻度的步长
  
  ```javascript
  // step0 是生成指定数量的刻度的间隔
  // step1 是最后生成的刻度的间隔
  // 我们希望 step1 满足两个条件：
  // 1. step1 = 10 ^ n * b (其中 b=1,2,5)
  // 2. step0 和 step1 的误差尽量的小
  export function tickStep(min, max, count) {
    const e10 = Math.sqrt(50); // 7.07
    const e5 = Math.sqrt(10); // 3.16
    const e2 = Math.sqrt(2); // 1.41
  
    // 获得目标间隔 step0，设 step0 = 10 ^ m
    const step0 = Math.abs(max - min) / Math.max(0, count);
    // 获得 step1 的初始值 = 10 ^ n < step0，其中 n 为满足条件的最大整数
    let step1 = 10 ** Math.floor(Math.log(step0) / Math.LN10);
    // 计算 step1 和 step0 的误差，error = 10 ^ m / 10 ^ n = 10 ^ (m - n)
    const error = step0 / step1;
    // 根据当前的误差改变 step1 的值，从而减少误差
    // 1. 当 m - n >= 0.85 = log(e10) 的时候，step1 * 10
    // 可以减少log(10) = 1 的误差 
    if (error >= e10) step1 *= 10;
    // 2. 当 0.85 > m - n >= 0.5 = log(e5) 的时候，step1 * 5
    // 可以减少 log(5) = 0.7 的误差
    else if (error >= e5) step1 *= 5;
    // 3. 当 0.5 > m - n >= 0.15 = log(e2) 的时候，step1 * 2
    // 那么可以减少 log(2) = 0.3 的误差
    else if (error >= e2) step1 *= 2;
    // 4. 当 0.15 > m - n > 0 的时候，step1 * 1
    return step1;
  }
  ```
  
  获得刻度数组
  
  ```javascript
  export function ticks(min, max, count) {
    const step = tickStep(min, max, count);
    // 让 start 和 stop 都是 step 的整数倍
    // 这样生成的 ticks 都是 step 的整数倍
    // 可以让可读性更强
    const start = Math.ceil(min / step);
    const stop = Math.floor(max / step);
    const n = Math.ceil(stop - start + 1);
    // n 不一定等于 count，所以生成的 ticks 的数量可能和指定的不一样
    const values = new Array(n);
    for (let i = 0; i < n; i += 1) {
      values[i] = round((start + i) * step);
    }
    return values;
  }
  
  // 简单解决 js 的精读问题：0.1 + 0.2 !== 0.3
  export function round(n) {
    return Math.round(n * 1e12) / 1e12;
  }
  ```
  
  range的最大值，最小值也处理一下,使得最小值和最大值都是刻度间隔的整数倍
  
  ``` javascript
  export function nice(domain, interval) {
    const [min, max] = domain;
    return [interval.floor(min), interval.ceil(max)];
  }
  
  export function ceil(n, base) {
    return base * Math.ceil(n / base);
  }
  
  export function floor(n, base) {
    return base * Math.floor(n / base);
  }
  
  let d0 = 0.1;
  let d1 = 9.9;
  const step = tickStep(d0, d1, tickCount);
  [d0, d1] = nice([0.1, 9.9], {
    floor: (x) => floor(x, step),
    ceil: (x) => ceil(x, step),
  });
  
  d0; // 0
  d1; // 10
  ticks(d0, d1, 6); // [0, 2, 4, 6, 8]
  ```
  
  

### 坐标系

位置，大小，一系列坐标点的转换函数



### 几何图形

 位置（Position)、大小（Size）和倾斜角度（Tilt） 

形状 颜色


    - 圆圈
    - 线
    - 矩形
    - 文字

### 辅助工具

