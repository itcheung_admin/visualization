export function descendants(root) {
  const nodes = [];
  const push = (d) => nodes.push(d);
  bfs(root, push);
  return nodes;
}

//  宽度优先搜索算法（又称广度优先搜索）是最简便的图的搜索算法之一
export function bfs(root, callback) {
  const discovered = [root];
  while (discovered.length) {
    const node = discovered.pop();
    callback(node);
    console.log('node' ,);
    console.log(node);
    discovered.push(...(node.children || []));
  }
}
