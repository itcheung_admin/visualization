import { createViews } from '../view';
import { createRenderer } from '../renderer';
import { createCoordinate } from '../coordinate';
import { create } from './create';
import { inferScales, applyScales } from './scale';
import { initialize } from './geometry';
import { inferGuides } from './guide';
import { bfs, identity, map, assignDefined } from '../utils';

export function plot(root) {
  console.log(root)
  debugger
  const { width = 640, height = 480, renderer: plugin } = root;
  const renderer = createRenderer(width, height, plugin);
  console.log('-renderer------------------')
  console.log(renderer)
  // / 将配置从容器节点流向视图节点
  flow(root);
  // 将视图树转变为视图数组
  const views = createViews(root);
  for (const [view, nodes] of views) {
    const { transform = identity, ...dimensions } = view;
    debugger
    console.log('transform' , transform);
    console.log('identity' , identity);
    console.log('dimensions' , dimensions);
    const geometries = [];
    const scales = {};
    const guides = {};
    let coordinates = [];
    const chartNodes = nodes.filter(({ type }) => isChartNode(type));
    // 合并同一区域的所拥有视图的配置
    for (const options of chartNodes) {
      const {
        scales: s = {},
        guides: g = {},
        coordinates: c = [],
        transforms = [],
        paddingLeft, paddingRight, paddingBottom, paddingTop,
        ...geometry
      } = options;
      assignDefined(scales, s);
      assignDefined(guides, g);
      // 合并 padding 等配置
      assignDefined(dimensions, { paddingLeft, paddingRight, paddingBottom, paddingTop });
      if (c) coordinates = c;
      geometries.push({ ...geometry, transforms: [transform, ...transforms] });
    }
    console.log('renderer,scales,guides,geometries,coordinates,dimensions');
    console.log('renderer',renderer);
    console.log('scales',scales);
    console.log('guides',guides);
    console.log('geometries',geometries);
    console.log('coordinates',coordinates);
    console.log('dimensions',dimensions);

    plotView({ renderer, scales, guides, geometries, coordinates, ...dimensions });
  }
  return renderer.node();
}

//  plotView 函数，该函数是真正把图表渲染出来的地方
// 第一个就是 initialize 函数，这是获取每个几何图形通道值的地方；

// 第二就是 inferScales 这个函数，这是给每个通道选择比例尺的地方，只要比例尺选择对了，那么绘制的几何图形就基本上没有问题了。
function plotView({
  renderer,
  scales: scalesOptions,
  guides: guidesOptions,
  coordinates: coordinateOptions,
  geometries: geometriesOptions,
  width, height, x, y,
  paddingLeft = 45, paddingRight = 45, paddingBottom = 45, paddingTop = 65,
}) {
   // 获得每个通道的值
  const geometries = geometriesOptions.map(initialize);
  console.log('geometries');
  console.log(geometries);
  debugger
  const channels = geometries.map((d) => d.channels);

  // 推断 scales 和 guides
  const scaleDescriptors = inferScales(channels, scalesOptions);
  const guidesDescriptors = inferGuides(scaleDescriptors, { x, y, paddingLeft }, guidesOptions);
  // 生成 scales 和 guides
  const scales = map(scaleDescriptors, create);
  const guides = map(guidesDescriptors, create);
  // 生成坐标系
  const transforms = inferCoordinates(coordinateOptions).map(create);
  const coordinate = createCoordinate({
    x: x + paddingLeft,
    y: y + paddingTop,
    width: width - paddingLeft - paddingRight,
    height: height - paddingTop - paddingBottom,
    transforms,
  });
  // 绘制辅助组件
  for (const [key, guide] of Object.entries(guides)) {
    const scale = scales[key];
    guide(renderer, scale, coordinate);
  }
  // 绘制几何元素
  for (const { index, geometry, channels, styles } of geometries) {
    const values = applyScales(channels, scales);
    geometry(renderer, index, scales, values, styles, coordinate);
  }
}

function isChartNode(type) {
  switch (type) {
    case 'layer': case 'col': case 'row': return false;
    default:
      return true;
  }
}

function flow(root) {
  bfs(root, ({ type, children, ...options }) => {
    if (isChartNode(type)) return;
    if (!children || children.length === 0) return;
    const keyDescriptors = [
      'o:encodings', 'o:scales', 'o:guides', 'o:styles',
      'a:coordinates', 'a:statistics', 'a:transforms', 'a:data',
    ];
    for (const child of children) {
      for (const descriptor of keyDescriptors) {
        const [type, key] = descriptor.split(':');
        if (type === 'o') {
          child[key] = { ...options[key], ...child[key] };
        } else {
          child[key] = child[key] || options[key];
        }
      }
    }
  });
}

function inferCoordinates(coordinates) {
  return [...coordinates, { type: 'cartesian' }];
}
