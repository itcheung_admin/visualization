import { plot } from "./src/index.js";

// const data = [
//   { genre: "Sports", sold: 275 },
//   { genre: "Strategy", sold: 115 },
//   { genre: "Action", sold: 120 },
//   { genre: "Shooter", sold: 350 },
//   { genre: "Other", sold: 150 },
// ];
// const chart = plot({
//   data,
//   type: "interval",
//   encodings: {
//     x: "genre",
//     y: "sold",
//     fill: "genre",
//   },
// });
// var node = document.createElement("LI");
// var textnode = document.createTextNode("乒乓球");
// node.appendChild(textnode);
// document.querySelector("body").appendChild(chart);

(async () => {
    const response = await fetch(
      "https://gw.alipayobjects.com/os/antvdemo/assets/data/scatter.json"
    );
    const data = await response.json();
    const chart = plot({
      type: "rect",
      // data:data.filter((item,i)=> i%3 === 0 ),
      data:data.filter((item,i)=> i<5 ),
      scales: {
        y: { label: "count" },
      },
      paddingTop: 30,
      statistics: [{ type: "binX", channel: "y" }],
      encodings: {
        x: "height",
      },
    });
    document.querySelector("body").appendChild(chart);
  })();


