import { createGeometry } from "./geometry";
import { line as drawLine } from "./line";
import { createChannels } from "./channel";

export function line() {
  return createGeometry(createChannels, drawLine);
}
