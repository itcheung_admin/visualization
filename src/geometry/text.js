
export function drawFillText(
  context,
  x,
  y,
  text,
  color= "red",
  align = "left",
  baseline = "top",
  font = "10px sans-serif"
) {
    
  if (context !== null) {
    context.save();
    context.textAlign = align;
    context.textBaseline = baseline;
    context.font = font;
    context.fillStyle = color;
    context.fillText(text, x, y);
    context.restore();
  }
}
