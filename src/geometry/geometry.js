
export function createGeometry(channels, render) {
  if (!channels) return new Error("channels null", channels);

  const geometry = (renderer, I, scales, values, styles, coordinate) => {
    return render(renderer, I, scales, values, styles, coordinate);
  };

  geometry.channels = () => channels;

  return geometry;
}
