
 /**
 * @example  drawLineDashRect(context, 1, 1, 100, 1);
 * @param context 
 * @param x 
 * @param y 
 * @param w 
 * @param h 
 */

 export function drawLineDashRect ( context, x, y, w, h ){
    if ( context !== null ) {
  
        context.save();
        context.lineWidth = 2;
        context.strokeStyle = 'rgb( 0 , 0 , 255 )';
  
        context.setLineDash( [ 10, 5 ] );
 
        context.beginPath();
        context.moveTo( x, y );
        context.lineTo( x + w, y );
        context.lineTo( x + w, y + h );
        context.lineTo( x, y + h );
        context.closePath();
        context.fill();
        context.stroke();
        context.restore();
    }
}