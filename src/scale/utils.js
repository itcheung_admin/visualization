/**
 * 
 * @describe 归一化 计算输入的值到两端的比例。 输入的值到两端的比例，应该和输出到两端的比例相同
 * @param value 输入的值
 * @param start 区间起点
 * @param stop 区间结束点
 * @returns 返回输入的值到两端的比例
 */
export function normalize(value, start, stop) {
    return (value - start) / (stop - start);
}

/**
 * 
 * @describe 返回步长
 * @param {*} min 
 * @param {*} max 
 * @param {*} count 
 * @returns 
 */
export function tickStep(min, max, count) {

    const e10 = Math.sqrt(50) // 7.07
    const e5 = Math.sqrt(10) // 3.16
    const e2 = Math.sqrt(2) // 1.41

    // 获得目标间隔  step0
    const step0 = Math.abs(max - min) / Math.max(0, count)

    // 获得 step1 的初始值 = 10 ^ n < step0，其中 n 为满足条件的最大整数
    let step1 = 10 ** Math.floor(Math.log(step0) / Math.LN10);
    // 计算 step1 和 step0 的误差，error = 10 ^ m / 10 ^ n = 10 ^ (m - n)
    const error = step0 / step1;
    // 根据当前的误差改变 step1 的值，从而减少误差
    // 1. 当 m - n >= 0.85 = log(e10) 的时候，step1 * 10
    // 可以减少log(10) = 1 的误差 
    if (error >= e10) step1 *= 10;
    // 2. 当 0.85 > m - n >= 0.5 = log(e5) 的时候，step1 * 5
    // 可以减少 log(5) = 0.7 的误差
    else if (error >= e5) step1 *= 5;
    // 3. 当 0.5 > m - n >= 0.15 = log(e2) 的时候，step1 * 2
    // 那么可以减少 log(2) = 0.3 的误差
    else if (error >= e2) step1 *= 2;
    // 4. 当 0.15 > m - n > 0 的时候，step1 * 1
    return step1;

}


/**
 * 
 * @param {*} min 区间最小值
 * @param {*} max 区间最大值
 * @param {*} count 区间分几块
 * @returns {Array} values 每个坐标点集合
 */
export function ticks(min, max, count) {
    const step = tickStep(min, max, count)

    const start = Math.ceil(min / step)
    const stop = Math.ceil(max / step)

    const n = Math.ceil(stop - start + 1)

    const values = new Array(n)
    for (let index = 0; index < n; index += 1) {
        values[index] = round((start + index) * step)
    }

    return values
}

/**
 * 解决精度问题
 * @param {*} n 
 * @returns 
 */
export function round(n) {
    return Math.round(n * 1e12) / 1e12
}


export function nice(domain, interval) {
    const [min, max] = domain;
    return [interval.floor(min), interval.ceil(max)];
}

export function ceil(n, base) {
    return base * Math.ceil(n / base);
}

export function floor(n, base) {
    return base * Math.floor(n / base);
}