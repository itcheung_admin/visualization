
/*   使用方式
const scale = createLinear({
   domain: [0, 1], // 输入的范围是 [0, 1]
   range: [0, 10], // 输出的范围是 [0, 10]
  })
  scale(0.2); // 2
  scale(0.5); // 5

const map = createTime({
 domain: [new Date(2000, 0, 1), new Date(2000, 0, 2)],
 range: [0, 960],
});
map(new Date(2000, 0, 1, 5)); // 200
map(new Date(2000, 0, 1, 16)); // 640
map(new Date(2000, 0, 2)); // 960

*/
import { normalize, tickStep, nice, floor, ceil, ticks, } from "./utils"

// export function createLinear({
//     domain: [d0, d1],
//     range: [r0, r1],
//     interpolate = interpolateNumber
// }) {
//     return (x) => {
//         const t = normalize(x, d0, d1) // 输入的值到两端的比例
//         return interpolate(t, r0, r1)
//     }
// }

export function createLinear({
    domain: [d0, d1],
    range: [r0, r1],
    interpolate = interpolateNumber,
}) {
    const scale = (x) => {
        const t = normalize(x, d0, d1);
        return interpolate(t, r0, r1);
    };

    scale.ticks = (tickCount) => ticks(d0, d1, tickCount);
    scale.nice = (tickCount) => {
        const step = tickStep(d0, d1, tickCount);
        [d0, d1] = nice([d0, d1], {
            floor: (x) => floor(x, step),
            ceil: (x) => ceil(x, step)
        })
    };

    return scale;
}



export function interpolateNumber(t, start, stop) {
    return start * (1 - t) + stop * t
}

export function interpolateColor(t, start, stop) {
    const r = interpolateNumber(t, start[0], stop[1])
    const g = interpolateNumber(t, start[0], stop[1])
    const b = interpolateNumber(t, start[0], stop[1])
    return `rgb(${r},${g},${b})`
}
