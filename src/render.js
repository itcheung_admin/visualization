let canvas = document.createElement("canvas");
export function renderContext(){
  console.log(" renderContext --- ");

  let context= canvas.getContext("2d");
  if (context === null) return alert("不兼容canvas");
  return  context;
}

import { createLinear} from './scale/linear'

const scale = createLinear({
  domain:[0,1],
  range:[0,100]
})

document.querySelector('body').appendChild(canvas)
