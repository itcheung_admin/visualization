import { mount } from "../../sparrow/src/utils";

export function createContext(width, height) {
  const canvas = document.createElement("canvas");
  const context = canvas.getContext("2d");
  if (!context) return;
  canvas.width = width;
  canvas.height = height;

  return context;
}

export function mount(parent, child) {
  if (parent) {
    parent.appendChild(child);
  }
}
