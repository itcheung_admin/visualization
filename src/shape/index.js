import { renderContext } from "../render";
import { drawRect } from "./rect";
import { drawLineDashRect } from "./line";
import { drawStrokeCircle } from "./circle.js";
import { drawFillText } from "./text";

const context = renderContext();
const drawShape = {
  line: drawLineDashRect(context, 1, 1, 1020, 1),
  circle: drawStrokeCircle(context, 100, 100, 100),
  text: drawFillText(context, 100, 100, "中文"),
  rect: drawRect(context, 10, 10, 100, 10),
};

/**
 * @param shape ['line'|'rect'|'text'|'circle']
 */
export const createGeometry = (shape, options) => {
  // return drawShape[shape](createChannels(options));
  return drawShape[shape];
};

/**
 * @description 处理数据 坐标系 比例尺 等
 * @description name	属性的名字	否	-
 * @description optional	values 里面是否需要该属性对应的值	否	true
 * @description scale	需要使用的比例尺	是
 */
export function createChannels(options) {
  return {
    x: createChannel({ name: "x", optional: false }), // x 坐标
    y: createChannel({ name: "y", optional: false }), // y 坐标
    stroke: createChannel({ name: "stroke" }), // 边框颜色
    fill: createChannel({ name: "fill" }), // 填充颜色
    ...options,
  };
}
