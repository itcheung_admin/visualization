export function drawRect(
  context,
  x,
  y,
  w,
  h
) {
  if (context !== null) {
    context.save();
    context.lineWidth = 20;

    // 绘制圆形的结束线
    // this . context2D . lineCap = 'round';
    // this . context2D . lineJoin = 'miter' ;
    // this . context2D . miterLimit = 1.3 ;
    context.strokeStyle = "blue";
    context.fillStyle = "grey";
    context.beginPath();
    context.moveTo(x, y);
    context.lineTo(x + w, y);
    context.lineTo(x + w, y + h);
    context.lineTo(x, y + h);
    context.closePath();
    context.fill();
    context.stroke();
    context.restore();
  }
}
