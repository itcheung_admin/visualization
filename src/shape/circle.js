export function drawFillCircle(context, x, y, radius, fillStyle = 'red') {
    if (context !== null) {
        context.save();
        context.fillStyle = fillStyle;
        context.beginPath();
        context.arc(x, y, radius, 0, Math.PI * 2);
        context.fill();
        context.restore();
    }
}

export function drawStrokeCircle(context, x, y, radius, color = 'red', lineWidth = 1) {
    if (context !== null) {
        context.save();
        context.strokeStyle = color;
        context.lineWidth = lineWidth;
        context.beginPath();
        context.arc(x, y, radius, 0, Math.PI * 2);
        context.stroke();
        context.restore();
    }
}