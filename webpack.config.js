const path = require("path");
const htmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  mode: "development",
  entry: {
    //  三个入口文件
    index: "./src/index.js",
    sparrow: "./sparrow/index.js",
  },
  output: {
    filename: "[name].js",
    path: path.join(__dirname, "./dist"),
    library: {
      name: "_",
      type: "umd",
    },
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: ["babel-loader"],
        // options: {
        //     presets: ['@babel/preset-env'],
        //   },
      },
      {
        test: /\.(csv|tsv)$/,
        use: ["csv-loader"],
      },
      {
        test: /\.xml$/,
        use: ["xml-loader"],
      },
    ],
  },
  plugins: [
    new htmlWebpackPlugin({
      template: "./public/index.html",
      filename: "index.html",
      chunks: ["index"],
      hot: true,
    }),
    new htmlWebpackPlugin({
      //HTML模板路径
      template: "./sparrow/index.html",
      filename: "sparrow.html",
      chunks: ["sparrow"],
      hot: true,
    }),
  ],
  devtool: "source-map",
};
